<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php include_once('metatags.php') ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title();?></title>
    <?php include_once('favicon.php'); ?>
    <?php include_once('commonStyles.php'); ?>
    <link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/greu-proyects.min.css">
</head>
<body class="greu-proyects projects-home greu-global fadeIn">
<div class="row content-site">
    <?php
        /*
        template name: Proyects home
        */ 
        include_once('header-two.php'); 
    ?>
    <section class="small-12 medium-8 large-6 column content-site__section-two">
        <article class="content-proyects-list">
            <ul>
                <?php
                    // set the "paged" parameter (use 'page' if the query is on a static front page)
                    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

                    // the query
                    $query = new WP_Query( array( 'cat' => 4 ) );
                    // $the_query = new WP_Query( 'cat=4&paged=' . $paged ); 
                    ?>
                    <?php if ( $the_query->have_posts() ) : ?>
                    <?php
                    // the loop
                    while ( $the_query->have_posts() ) : $the_query->the_post(); 
                    ?>
                    <li class="project_1 project-card large-4">
                        <a href="<?php the_permalink(); ?>">
                            <figure>
                                <picture>
                                    <?php if ( has_post_thumbnail() ) { the_post_thumbnail('list_article_thumbs'); } ?>
                                </picture>
                                <figcaption>
                                    <h2><?php the_title(); ?></h2>
                                </figcaption>
                            </figure>
                        </a>
                    </li>
                    <?php endwhile; ?>
                    <?php else:  ?>
                    <li><?php _e( 'Sorry, no posts matched your criteria.' ); ?></li>
                <?php endif; ?>
            </ul>
        </article>
    </section>
</div>
<footer style="display:none;">
<?php get_footer(); ?>
</footer>
<script src="<?php bloginfo('template_url')?>/js/greu-projects.bundle.min.js"></script>
</body>
</html>