var $ = require("jquery");
var $fq = require("jquery");
window.fq = $fq;
var GlobalFunctions = function(){
    // Variables globales.
    // container se declara global para optimizar función
    // enableHover en evento mousemove.
    var hasHoverClass = false;
    var lastTouchTime = 0;
    var container = $("body");

    var init = function (){
        // Listeners de eventos.
        // Reconocimiento de despositivo táctil en tiempo real.
        $(document).on("touchstart", updateLastTouchTime);
        $(document).on("touchstart", disableHover);
        // No se usan los eventos de jQuery para mejorar performance.
        document.addEventListener("mousemove", enableHover, true);


    };
    // Si reconoce que tiene hover añade la clase body--has-hover al body.
    var enableHover = function () {
        // filter emulated events coming from touch events
        if (new Date() - lastTouchTime < 500) return;
        if (hasHoverClass) return;
        container.addClass("body--has-hover");
        hasHoverClass = true;
    };

    // Si reconoce que tiene touch retira la clase body--has-hover del body.
    var disableHover = function () {
        if (!hasHoverClass) return;
        container.removeClass("body--has-hover");
        hasHoverClass = false;
    };

    // Crea un timestamp para usarse en enableHover.
    var updateLastTouchTime = function () {
        lastTouchTime = new Date();
    };
    return{init:init , hasHoverClass:hasHoverClass};
    $( document ).ready(function() {
       $('body').show();
    });
}();
module.exports.GlobalFunctions = GlobalFunctions.init();