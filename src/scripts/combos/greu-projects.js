var jsCookie = require("js-cookie"); // No necesita jQuery
var $fq = require("jquery");
window.fq = $fq;
var slick = require("slick-carousel");
//Files get to base/->
var globales = require("../base/global");

var funcionesProject = function(){
    var listProjects;
    var init = function () {
        listProjects = fq('.content-proyects-list > ul');
        sliderMobileListProjects();
    };
    var sliderMobileListProjects = function () {
        listProjects.slick({
            dots: false,
            infinite: true,
            speed: 300,
            prevArrow: '<button type="button" class="slick-prev "><i class="icon-across-to-left"></i></button>',
            nextArrow: '<button type="button" class="slick-next "><i class="icon-across-to-right"></i></button>',
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [
                {
                    breakpoint: 1920,
                    settings: 'unslick'
                },
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    };
    return{init:init};
}();
fq(document).ready(function() {
    funcionesProject.init();
});