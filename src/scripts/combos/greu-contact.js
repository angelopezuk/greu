var $ = require("jquery");
require("jquery-validation");
// window.jq = $jq; // Para ser usado por el código de Fancybox 3. Ver string-replace-loader en webpack.config.
var globales = require("../base/global");
var FuncionesGeneralesContact = function(){
	var init = function(){
		// $('form').validate({
		// 	debug: true
		// });
		formContact();
	}
	var formContact = function(){
		console.log('nuew function');
		$("#contact-form").validate({
			rules:{
				'contact-name':{
					required  : true,
					minlength : 2
				},
				// 'contact-last-name':{
				// 	required  : true,
				// 	minlength : 2
				// },
				'contact-phone':{
					required  : true,
					number    : true,
					minlength : 2
				},
				'contact-email':{
					required  : true,
					email: true
				}
				// 'contact-city':{
				// 	required  : true	
				// }
			},
			messages:{
				'contact-name':{
					'required'	: 'Digite su nombre',
					'minlength' : 'Debes escribir más de 2 caracteres'
				},
				'contact-phone':{
					'required'  : 'Digita tu número de contacto',
					'number' 	: 'Debes escribir solo digitos',
					'minlength' : 'Debes escribir más de 2 caracteres numericos'
				},
				'contact-email':{
					'required'  : 'Digita tu email de contacto',
					'email'		: 'Escribe un email valido'
				}
			},
			submitHandler: function(form){
				// var datosLlamada = {
				// 	number  : fieldNumCel.val(),
				// 	queue   : queueDefault,
				// 	country : countryDefault
				// };
				// execCallMeBack(datosLlamada);
				$('form').submit(function(e) {
					e.preventDefault();
					console.log('submit!!');
					var url = 'http://www.greuarquitectos.com/contactForm/formContacts.php';
					// var data = $(this).serializeArray();
					var Name = $('#contact-name').val();
					var lastName = $('#contact-last-name').val();
					var phoneNumber = $('#contact-phone').val();
					var email = $('#contact-email').val();
					var city = $('#contact-city').val();

					var data = 'Name=' + Name + '&lastName=' + lastName + '&phoneNumber=' + phoneNumber + '&email=' + email + '&email=' + email + '&city=' + city;
					// var data = {Name:Name ,lastName:lastName,phoneNumber:phoneNumber,email:email,email:email,city:city};

					console.log(data);

					$.ajax({
					url: url,
					type: 'POST',
					// dataType: 'json',
					data: data,
					cache: false,
				    data: data,
				    // contentType: false,
				    processData: false,
					// beforeSend: function() {
					// 	console.log('before');
					// },
				        success:function(datos){
				          	console.log(datos);
				       		console.log('entra');
				       		$('button').after('<span style="display:block;">'+datos+'</span>');
				       		$("form")[0].reset();
				        },
				        error:function(datos){
				        	console.log('hay error');
				        	// // var datosTwo = JSON.parse(datos);
				        	// $.each(datos, function(i,v){
				        	// 	// console.log(i);
				        	// });
				        	$('button').after('<span style="display:block;">' + 'Hay un error en el sistema' + '</span>');

				        } 
					})
					// .done(function(data) {
					//   console.log('done');
					// })
					// .fail(function(data) {
					//     console.log( "error" );
					//     console.log(data);
				 //  	})
				});
		    }
		});
		var execContactForm = function(){
			$('form').submit(function(e) {
				e.preventDefault();
				console.log('submit!!');
				var url = 'http://www.greuarquitectos.com/contactForm/formContacts.php';
				// var data = $(this).serializeArray();
				var Name = $('#contact-name').val();
				var lastName = $('#contact-last-name').val();
				var phoneNumber = $('#contact-phone').val();
				var email = $('#contact-email').val();
				var city = $('#contact-city').val();

				var data = 'Name=' + Name + '&lastName=' + lastName + '&phoneNumber=' + phoneNumber + '&email=' + email + '&email=' + email + '&city=' + city;
				// var data = {Name:Name ,lastName:lastName,phoneNumber:phoneNumber,email:email,email:email,city:city};

				console.log(data);

				$.ajax({
				url: url,
				type: 'POST',
				// dataType: 'json',
				data: data,
				cache: false,
			    data: data,
			    // contentType: false,
			    processData: false,
				// beforeSend: function() {
				// 	console.log('before');
				// },
			        success:function(datos){
			          	console.log(datos);
			       		console.log('entra');
			       		$('button').after(datos);
			       		$("form")[0].reset();
			        },
			        error:function(datos){
			        	console.log('hay error');
			        	// // var datosTwo = JSON.parse(datos);
			        	// $.each(datos, function(i,v){
			        	// 	// console.log(i);
			        	// });
			        	$('button').after('<span style="display:block;">' + 'Hay error' + '</span>');

			        } 
				})
				// .done(function(data) {
				//   console.log('done');
				// })
				// .fail(function(data) {
				//     console.log( "error" );
				//     console.log(data);
			 //  	})
			});
		}
	};
	return{init:init};
}();
$(document).ready(function(){
	console.log('run contact!!');
	FuncionesGeneralesContact.init();
});