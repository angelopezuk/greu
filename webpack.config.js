const path = require('path');
uglifyjs = require('uglifyjs-webpack-plugin')

module.exports = {
    entry: { // se pueden especificar varos entry points
        "greu-home": "./src/scripts/combos/greu-home.js",
        "greu-projects": "./src/scripts/combos/greu-projects.js",
        "greu-contact": "./src/scripts/combos/greu-contact.js"
    }, // solo se puede especificar una carpeta output
    output: {
        filename: '[name].bundle.min.js',
        path: path.resolve(__dirname, 'src/scripts/outputs')
    },
    devtool: "source-map",
    watch: true,
    plugins: [
        new uglifyjs({
            sourceMap: true
        })
    ]
};